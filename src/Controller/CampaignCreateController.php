<?php

namespace App\Controller;

use App\Form\CampaignCreateDTO;
use App\Form\CampaignCreateFormType;
use App\Service\CampaignCreator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CampaignCreateController extends AbstractController
{
    public function index(
        Request $request,
        CampaignCreator $campaignCreator
    ): Response {
        $dto = new CampaignCreateDTO();
        $form = $this->createForm(CampaignCreateFormType::class, $dto);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $campaigns = $campaignCreator->create($this->getUser(), $dto);

                foreach ($campaigns as $campaign) {
                    $this->addFlash('campaign.create.success', sprintf(
                        'Campaign create success: %s',
                        $campaign->getResourceName()
                    ));
                }
            } catch (\Exception $e) {
                $this->addFlash('campaign.create.error', $e->getMessage());
            }
        }

        return $this->render('campaign_create/index.html.twig', [
            'campaignCreateForm' => $form->createView(),
        ]);
    }
}
