<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Form\CampaignCreateDTO;
use Google\Ads\GoogleAds\Lib\OAuth2TokenBuilder;
use Google\Ads\GoogleAds\Lib\V6\GoogleAdsClient;
use Google\Ads\GoogleAds\Lib\V6\GoogleAdsClientBuilder;
use Google\Ads\GoogleAds\V6\Common\ManualCpc;
use Google\Ads\GoogleAds\V6\Enums\AdvertisingChannelTypeEnum\AdvertisingChannelType;
use Google\Ads\GoogleAds\V6\Enums\BudgetDeliveryMethodEnum\BudgetDeliveryMethod;
use Google\Ads\GoogleAds\V6\Enums\CampaignStatusEnum\CampaignStatus;
use Google\Ads\GoogleAds\V6\Resources\Campaign;
use Google\Ads\GoogleAds\V6\Resources\Campaign\NetworkSettings;
use Google\Ads\GoogleAds\V6\Resources\CampaignBudget;
use Google\Ads\GoogleAds\V6\Services\CampaignBudgetOperation;
use Google\Ads\GoogleAds\V6\Services\CampaignOperation;
use Google\Ads\GoogleAds\V6\Services\MutateCampaignBudgetResult;
use Google\Protobuf\Internal\RepeatedField;

final class CampaignCreator
{
    private string $clientId;
    private string $clientSecret;
    private string $developerToken;

    public function __construct(
        string $clientId,
        string $clientSecret,
        string $developerToken
    ) {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->developerToken = $developerToken;
    }

    public function create(User $user, CampaignCreateDTO $dto): RepeatedField
    {
        $client = $this->getClient($user, $dto);
        $budget = $this->createBudget($client, $dto);

        $networkSettings = new NetworkSettings([
            'target_google_search' => true,
            'target_search_network' => true,
            'target_content_network' => false,
            'target_partner_search_network' => false
        ]);

        $campaign = new Campaign([
            'name'                     => $dto->getName(),
            'advertising_channel_type' => AdvertisingChannelType::SEARCH,
            'status'                   => CampaignStatus::PAUSED,
            'manual_cpc'               => new ManualCpc(),
            'campaign_budget'          => $budget->getResourceName(),
            'network_settings'         => $networkSettings,
            'start_date'               => date('Ymd', strtotime('+1 day')),
            'end_date'                 => date('Ymd', strtotime('+1 month'))
        ]);

        $campaignOperation = new CampaignOperation();
        $campaignOperation->setCreate($campaign);
        $campaignOperations[] = $campaignOperation;

        $campaignServiceClient = $client->getCampaignServiceClient();
        $response = $campaignServiceClient->mutateCampaigns($dto->getCustomerId(), $campaignOperations);

        return $response->getResults();
    }

    private function createBudget(GoogleAdsClient $client, CampaignCreateDTO $dto): MutateCampaignBudgetResult
    {
        $budget = new CampaignBudget([
            'name'            => $dto->getBudgetName(),
            'delivery_method' => BudgetDeliveryMethod::STANDARD,
            'amount_micros'   => $dto->getBudgetAmount(),
        ]);

        $campaignBudgetOperation = new CampaignBudgetOperation();
        $campaignBudgetOperation->setCreate($budget);

        $response = $client->getCampaignBudgetServiceClient()->mutateCampaignBudgets(
            $dto->getCustomerId(),
            [$campaignBudgetOperation]
        );

        return $response->getResults()[0];
    }

    private function getClient(User $user, CampaignCreateDTO $dto): GoogleAdsClient
    {
        $oAuth2Credential = (new OAuth2TokenBuilder())
            ->withClientId($this->clientId)
            ->withClientSecret($this->clientSecret)
            ->withRefreshToken($user->getRefreshToken())
            ->build();

        return (new GoogleAdsClientBuilder())
            ->withDeveloperToken($this->developerToken)
            ->withOAuth2Credential($oAuth2Credential)
            ->withLoginCustomerId($dto->getLoginCustomerId())
            ->build()
        ;
    }
}