<?php

declare(strict_types=1);

namespace App\Form;


class CampaignCreateDTO
{
    private string $name;

    private string $budgetName;

    private int $budgetAmount;

    private int $customerId;

    private ?int $loginCustomerId;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getBudgetName(): string
    {
        return $this->budgetName;
    }

    public function setBudgetName(string $budgetName): void
    {
        $this->budgetName = $budgetName;
    }

    public function getBudgetAmount(): int
    {
        return $this->budgetAmount;
    }

    public function setBudgetAmount(int $budgetAmount): void
    {
        $this->budgetAmount = $budgetAmount;
    }

    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    public function setCustomerId(int $customerId): void
    {
        $this->customerId = $customerId;
    }

    public function getLoginCustomerId(): ?int
    {
        return $this->loginCustomerId;
    }

    public function setLoginCustomerId(?int $loginCustomerId): void
    {
        $this->loginCustomerId = $loginCustomerId;
    }
}