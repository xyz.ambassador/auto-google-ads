<?php

namespace App\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class MenuBuilder
{
    private FactoryInterface $factory;

    private Security $security;

    public function __construct(
        FactoryInterface $factory,
        Security $security
    ) {
        $this->factory = $factory;
        $this->security = $security;
    }

    public function createLeftMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'navbar-nav mr-auto');

        if ($user = $this->security->getUser()) {
            $this->createLeftUserMenu($menu, $user);
        } else {
            $this->createLeftGuestMenu($menu);
        }

        foreach ($menu as $child) {
            $child->setLinkAttribute('class', 'nav-link')
                ->setAttribute('class', 'nav-item');
        }

        return $menu;
    }

    public function createRightMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'navbar-nav ml-auto');

        if ($user = $this->security->getUser()) {
            $this->createRightUserMenu($menu, $user);
        } else {
            $this->createRightGuestMenu($menu);
        }

        return $menu;
    }

    private function createLeftUserMenu(ItemInterface $menu,  UserInterface $user): void
    {
//        $menu->addChild('Accounts', ['route' => 'instagram.account']);
//        $menu->addChild('Proxies', ['route' => 'app.proxy']);
    }

    private function createLeftGuestMenu(ItemInterface $menu): void
    {
    }

    private function createRightUserMenu(ItemInterface $menu, UserInterface $user): void
    {
        $item = $menu->addChild($user->getUsername(), [
            'attributes' => [
                'class' => 'nav-item dropdown',
            ],
            'linkAttributes' => [
                'class' => 'nav-link dropdown-toggle',
                'role'  => 'button',
                'data-toggle' => 'dropdown',
            ],
            'uri' => '#',
        ]);

//        $item->addChild('Profile', [
//            'route' => 'fos_user_profile_show',
//            'attributes' => [
//                'id' => 'back_to_homepage'
//            ],
//        ]);

        $item->addChild('Logout', [
            'route' => 'logout',
            'attributes' => [
                'divider_prepend' => true,
                'icon' => 'fa fa-sign-out',
            ],
        ]);
    }

    private function createRightGuestMenu(ItemInterface $menu): void
    {
        $menu->addChild('Sign in', [
            'route' => 'login',
            'linkAttributes' => [
                'class' => 'nav-link',
            ],
        ]);
        $menu->addChild('Sign up', [
            'route' => 'register',
            'linkAttributes' => [
                'class' => 'nav-link',
            ],
        ]);
    }
}