# Auto Google Ads

## Features
* Login
* Registration
* Create Google Ads campaign

## Getting started

1.  Clone this project in the directory of your choice via:

        git clone git@gitlab.com:xyz.ambassador/auto-google-ads.git && cd auto-google-ads
        
2.  Set up environments in .env file:

        GOOGLE_API_CLIENT_ID=
        GOOGLE_API_CLIENT_SECRET=
        GOOGLE_API_DEVELOPER_TOKEN=
        
3.1  Build project with Makefile:

        make
    
3.2  Or use docker compose:

        docker network create google-ags-network
        docker-compose -f .infrastructure/docker-compose/docker-compose.yml -f .infrastructure/docker-compose/docker-compose.dev.yml up -d --build --remove-orphans
        docker exec -t php-fpm bash -c 'COMPOSER_MEMORY_LIMIT=-1 composer install'
        docker exec -t php-fpm bash -c 'bin/console doctrine:migrations:migrate -n'
    
4.  Open in your browser: https://localhost:8080/register